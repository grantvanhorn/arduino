# Arduino

## 1. Blink 

First program to test if the board is connected 

## 2. RGB

Make a light fade from one color to another.

## 3. Digital Input

Press a button to turn on the light. Press a different button to turn the light off.

![alt text](https://giant.gfycat.com/IdioticQuaintArrowana.gif "Logo Title Text 1")
