/*
Adafruit Arduino - Lesson 3. RGB LED
*/
 
int redPin = 6;
int greenPin = 5;
int bluePin = 3;

int redValue = 0;
int greenValue = 0;
int blueValue = 0;

int pinTurn = 0;
int increment = 1;

//uncomment this line if using a Common Anode LED
//#define COMMON_ANODE
 
void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);  
}
 
void loop()
{
//  setColor(255, 0, 0);  // red
//  delay(1000);
//  setColor(0, 255, 0);  // green
//  delay(1000);
//  setColor(0, 0, 255);  // blue
//  delay(1000);
//  setColor(255, 255, 0);  // yellow
//  delay(1000);  
//  setColor(80, 0, 80);  // purple
//  delay(1000);
//  setColor(0, 255, 255);  // aqua
//  delay(1000);
  incrementColor();
  delay(10);
}


void incrementColor()
{
  if(pinTurn == 0)
  {
    if (increment)
    {
      redValue++;
    } else 
    {
      redValue--;
    }
  
    if(redValue == 255)
    {
      increment = 0;
    }
  
    if(redValue == 0)
    {
      increment = 1;
      pinTurn = 1;
    }
  }

  // -------------------------

  if(pinTurn == 1)
  {
    if(increment)
    {
      greenValue++;
    } else 
    {
      greenValue--;
    }
  
    if(greenValue == 255)
    {
      increment = 0;
    }
  
    if(greenValue == 0)
    {
      increment = 1;
      pinTurn = 2;
    }
  }

  // ----------------------

  if(pinTurn == 2)
  {
    if (increment)
    {
      blueValue++;
    } else 
    {
      blueValue--;
    }
  
    if(blueValue == 255)
    {
      increment = 0;
    }
  
    if(blueValue == 0)
    {
      increment = 1;
      pinTurn = 0;
    }
  }

  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}

void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);  
}
